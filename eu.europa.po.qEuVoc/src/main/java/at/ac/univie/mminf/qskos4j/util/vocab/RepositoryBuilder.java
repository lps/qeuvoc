package at.ac.univie.mminf.qskos4j.util.vocab;

import org.junit.Assert;
import org.openrdf.OpenRDFException;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europa.po.proxy.ProxyConfigurationException;
import eu.europa.po.proxy.ProxySetup;
import eu.europa.po.qEuVoc.util.vocab.EuVocPrefix;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

//TODO: modify it to create a repository with all the vocabularies that may be involved
public class RepositoryBuilder {

    private final Logger logger = LoggerFactory
            .getLogger(RepositoryBuilder.class);

    private Repository repository;

    private static ProxySetup proxyInstance = null;

    public Repository setUpFromTestResource(String testFileName)
            throws OpenRDFException, IOException, URISyntaxException {
        this.setUpProxy();
        URL conceptsUrl = RepositoryBuilder.class
                .getResource("/" + testFileName);
        File conceptsFile = new File(conceptsUrl.getFile());
        Assert.assertNotNull(conceptsFile);
        repository = setUpFromFile(conceptsFile, null, RDFFormat.RDFXML);
        return repository;
    }

    public Repository setUpFromFile(File rdfFile, String baseURI,
            RDFFormat dataFormat) throws OpenRDFException, IOException, URISyntaxException {
        this.setUpProxy();
        logger.info("Initializing evaluation repository for "
                + rdfFile.getName() + "...");
        createRepositoryForFile();
        
        //addSkosOntology(); //replaced by adding the EuVoc concerned of ontologies 
        addOntologyModels();
        RepositoryConnection repCon = repository.getConnection();
        try {
            repCon.add(rdfFile, baseURI, dataFormat);
        } catch (Exception e) {
            throw new InvalidRdfException(
                    "Could not add RDF data from file to temporary repository");
        } finally {
            repCon.close();
        }

        return repository;
    }

    /**
     * Load all the EuVoc related Ontologies
     * @throws IOException 
     * @throws MalformedURLException 
     * @throws RepositoryException 
     * @throws RDFParseException 
     * @throws URISyntaxException 
     */
    protected void addOntologyModels() throws RDFParseException, RepositoryException, MalformedURLException, IOException, URISyntaxException {
        //adding SKOS
        repository.getConnection().add(
                EuVocPrefix.SKOS.getLocalResourceURL(),
                EuVocPrefix.SKOS.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.SKOS.getNameSpace()));
        //adding SKOS-XL
        repository.getConnection().add(
                EuVocPrefix.SKOSXL.getLocalResourceURL(),
                EuVocPrefix.SKOSXL.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.SKOSXL.getNameSpace()));
        //adding EUVOC
        repository.getConnection().add(
                EuVocPrefix.EUVOC.getLocalResourceURL(),
                EuVocPrefix.EUVOC.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.EUVOC.getNameSpace()));
        //adding DCAT
        repository.getConnection().add(
                EuVocPrefix.DCAT.getLocalResourceURL(),
                EuVocPrefix.DCAT.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.DCAT.getNameSpace()));
      //adding DCT
        repository.getConnection().add(
                EuVocPrefix.DCT.getLocalResourceURL(),
                EuVocPrefix.DCT.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.DCT.getNameSpace()));
      //adding DC
        repository.getConnection().add(
                EuVocPrefix.DC.getLocalResourceURL(),
                EuVocPrefix.DC.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.DC.getNameSpace()));
      //adding FOAF
        repository.getConnection().add(
                EuVocPrefix.FOAF.getLocalResourceURL(),
                EuVocPrefix.FOAF.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.FOAF.getNameSpace()));
      //adding VOID
        repository.getConnection().add(
                EuVocPrefix.VOID.getLocalResourceURL(),
                EuVocPrefix.VOID.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.VOID.getNameSpace()));
      //adding ADMS
        repository.getConnection().add(
                EuVocPrefix.ADMS.getLocalResourceURL(),
                EuVocPrefix.ADMS.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.ADMS.getNameSpace()));
      //adding LEMON
        repository.getConnection().add(
                EuVocPrefix.LEMON.getLocalResourceURL(),
                EuVocPrefix.LEMON.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.LEMON.getNameSpace()));
      //adding LEXVO
        repository.getConnection().add(
                EuVocPrefix.LEXVO.getLocalResourceURL(),
                EuVocPrefix.LEXVO.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.LEXVO.getNameSpace()));
      //adding LEXINFO
        repository.getConnection().add(
                EuVocPrefix.LEXINFO.getLocalResourceURL(),
                EuVocPrefix.LEXINFO.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.LEXINFO.getNameSpace()));
      //adding ORG
        repository.getConnection().add(
                EuVocPrefix.ORG.getLocalResourceURL(),
                EuVocPrefix.ORG.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.ORG.getNameSpace()));
      //adding PROV
        repository.getConnection().add(
                EuVocPrefix.PROV.getLocalResourceURL(),
                EuVocPrefix.PROV.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.PROV.getNameSpace()));
      //adding VCARD
        repository.getConnection().add(
                EuVocPrefix.VCARD.getLocalResourceURL(),
                EuVocPrefix.VCARD.getNameSpace(),
                RDFFormat.RDFXML,
                new URIImpl(EuVocPrefix.VCARD.getNameSpace()));
    }

    private void setUpProxy() {
        if (proxyInstance == null) {
            logger.info("Setting Up the Proxy configurations");
            proxyInstance = new ProxySetup();
            try {
                proxyInstance.setSystemProxy();
            } catch (ProxyConfigurationException e) {
                e.printStackTrace();
                logger.error("Failed to setUp the Proxy configurations");
            }
        }
    }

    private void createRepositoryForFile() throws RepositoryException {
        File tempDir = new File(createDataDirName());
        repository = new SailRepository(
                new ForwardChainingRDFSInferencer(new MemoryStore(tempDir)));
        repository.initialize();
    }

    @Deprecated
    private void addSkosOntology() throws OpenRDFException, IOException {
        repository.getConnection().add(new URL(SkosOntology.SKOS_ONTO_URI),
                SkosOntology.SKOS_BASE_URI, RDFFormat.RDFXML,
                new URIImpl(SkosOntology.SKOS_ONTO_URI));
    }

    private String createDataDirName() {
        return System.getProperty("java.io.tmpdir") + File.separator
                + System.currentTimeMillis();
    }

    /**
     * If this is called, the local repository is complemented with SKOS lexical
     * labels inferred from SKOSXL definitions as described in the SKOS
     * <a href="http://www.w3.org/TR/skos-reference/#S55">reference document</a>
     * by the axioms S55-S57
     *
     * @throws org.openrdf.OpenRDFException
     *             if errors when initializing local repository
     */
    @Deprecated
    public void enableSkosXlSupport() throws OpenRDFException {
        addSkosXlLabels("skosxl:prefLabel", "skos:prefLabel");
        addSkosXlLabels("skosxl:altLabel", "skos:altLabel");
        addSkosXlLabels("skosxl:hiddenLabel", "skos:hiddenLabel");
    }

    @Deprecated
    private void addSkosXlLabels(String skosXlProperty, String skosProperty)
            throws OpenRDFException {
        RepositoryConnection repCon = repository.getConnection();

        try {
            GraphQuery graphQuery = createSkosXlGraphQuery(repCon,
                    skosXlProperty, skosProperty);
            GraphQueryResult result = graphQuery.evaluate();

            while (result.hasNext()) {
                Statement statement = result.next();
                repCon.add(statement);
            }
        } finally {
            repCon.close();
        }
    }

    @Deprecated
    private GraphQuery createSkosXlGraphQuery(RepositoryConnection connection,
            String skosXlProperty, String skosProperty)
                    throws OpenRDFException {
        return connection.prepareGraphQuery(QueryLanguage.SPARQL,

        SparqlPrefix.SKOS + " " + SparqlPrefix.SKOSXL + "CONSTRUCT { ?concept "
                + skosProperty + " ?label }" + "WHERE {" + "?concept "
                + skosXlProperty + " ?xLabel ."
                + "?xLabel skosxl:literalForm ?label" + "}");
    }

}
