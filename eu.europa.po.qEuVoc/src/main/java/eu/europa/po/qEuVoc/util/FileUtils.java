/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 12 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author costeeu
 *
 */
public class FileUtils {

    public static String readTextFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static String readTextFile(URI path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static String readTextResourceFile(String resource)
            throws URISyntaxException, IOException {
        return readTextFile(getResource(resource),
                Charset.defaultCharset());
    }

    /**
     *  returns the URI of the resource
     * @param resource
     * @return
     * @throws URISyntaxException
     */
    public static URI getResource(String resource) throws URISyntaxException{
        return FileUtils.class.getClassLoader().getResource(resource).toURI();
    }

//    public static void main(String[] args) {
//        try {
//            // System.out.print(readFile("D:\\Repository\\qeuvoc\\eu.europa.po.qEuVoc\\src\\main\\resources\\conceptCardinality.rq",
//            // Charset.defaultCharset()));
//            System.out.print(readTextResourceFile("ontologies/adms.rdf"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
