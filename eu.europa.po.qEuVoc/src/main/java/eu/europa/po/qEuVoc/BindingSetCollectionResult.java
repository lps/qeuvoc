/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 8 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import org.openrdf.OpenRDFException;
import org.openrdf.query.BindingSet;
import org.openrdf.query.TupleQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.univie.mminf.qskos4j.result.CollectionResult;
import eu.europa.po.qEuVoc.util.QueryResultUtil;

/**
 * @author costeeu
 *
 */
public class BindingSetCollectionResult extends CollectionResult<BindingSet> {

    private final Logger logger = LoggerFactory
            .getLogger(BindingSetCollectionResult.class);

    protected Boolean problematic = null;

    /**
     * @param data
     * 
     *            represents a sparql result table with a set of rows each
     *            containing a set of records for the variables defined in the
     *            sparql query (BindingSet).
     */
    public BindingSetCollectionResult(Collection<BindingSet> data)
            throws OpenRDFException {
        super(data);
    }

    public BindingSetCollectionResult(TupleQueryResult data)
            throws OpenRDFException {
        super(QueryResultUtil.tupleQueryResultToCollection(data));
    }

    /*
     * (non-Javadoc)
     * 
     * @see at.ac.univie.mminf.qskos4j.result.Result#isProblematic()
     */
    @Override
    public boolean isProblematic() {
        if (this.problematic == null) return occurrenceCount() != 0;
        else return this.problematic;
    }

    /**
     * @param p
     */
    public void setProblematic(Boolean p) {
        this.problematic = p;
    }

    /*
     * (non-Javadoc)
     * 
     * @see at.ac.univie.mminf.qskos4j.result.CollectionResult#
     * generateExtensiveTextReport()
     */
    @Override
    public String generateExtensiveTextReport() {
        // StringBuilder report = new StringBuilder();
        CellStyle headderStyle = new CellStyle(HorizontalAlign.center);
        Table t = null;
        ArrayList<String> headder = null;
        for (BindingSet bs : getData()) {
            // creating a header and adding it to the table
            if (headder == null) {
                Set<String> bvalues = bs.getBindingNames();
                t = new Table(bvalues.size(), BorderStyle.DESIGN_FORMAL,
                        ShownBorders.ALL);
                headder = new ArrayList<>();
                for (String bVar : bvalues) {
                    headder.add(bVar);
                    t.addCell(bVar, headderStyle);
                }
            }
            // adding values to the table
            for (String bVar : headder) {
                String cellValue = "";
                if (bs.getValue(bVar) != null)
                    cellValue = bs.getValue(bVar).toString();
                // removing the Turtle type setting for data values
                int dataType = cellValue.indexOf("^^");
                if (dataType > 0) {
                    cellValue = cellValue.substring(0, dataType);
                    cellValue = cellValue.replace("\"", "");
                }
                t.addCell(cellValue);
            }
        }
        return t.render();
    }
}
