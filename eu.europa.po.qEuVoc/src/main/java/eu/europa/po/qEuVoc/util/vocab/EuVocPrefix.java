/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 8 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.util.vocab;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

import eu.europa.po.qEuVoc.util.FileUtils;

/**
 * @author costeeu
 *
 */
public enum EuVocPrefix {
    RDF("rdf", org.openrdf.model.vocabulary.RDF.NAMESPACE,null), 
    RDFS("rdfs",org.openrdf.model.vocabulary.RDFS.NAMESPACE,null), 
    OWL("owl",org.openrdf.model.vocabulary.OWL.NAMESPACE,null), 
    XSD("xsd",org.openrdf.model.vocabulary.XMLSchema.NAMESPACE,null),
    SKOS("skos", "http://www.w3.org/2004/02/skos/core#", "skos.rdf"), 
    SKOSXL("skosxl","http://www.w3.org/2008/05/skos-xl#","skos-xl.rdf"), 
    EUVOC("euvoc","http://publications.europa.eu/ontology/euvoc#","euvoc.rdf"),
    DCAT("dcat","http://www.w3.org/ns/dcat#","dcat.rdf"),
    DCT("dct", org.openrdf.model.vocabulary.DCTERMS.NAMESPACE,"dcterms.rdf"), 
    DC("dc",org.openrdf.model.vocabulary.DC.NAMESPACE,"dcelements.rdf"), 
    FOAF("foaf",org.openrdf.model.vocabulary.FOAF.NAMESPACE,"foaf.rdf"), 
    ADMS("adms","http://www.w3.org/ns/adms#","adms.rdf"),
    LEMON("lemon", "http://lemon-model.net/lemon#","lemon.rdf"), 
    LEXINFO("lexinfo","http://www.lexinfo.net/ontology/2.0/lexinfo#","lexinfo.owl"), 
    LEXVO("lexvo","http://lexvo.org/ontology#","lexvo.owl"), 
    ORG("org","http://www.w3.org/ns/org#","org.rdf"), 
    PROV("prov","http://www.w3.org/ns/prov#","prov-o.owl"), 
    VCARD("vcard","http://www.w3.org/2006/vcard/ns#","vcard.rdf"), 
    VOID("void","http://rdfs.org/ns/void#","void.owl");

    private final static String ontologiesProjectPath = "ontologies/";

    private String abbrv, nameSpace, localResourceName;

    private EuVocPrefix(String abbrv, String nameSpace,
            String localResourceName) {
        this.localResourceName = localResourceName;
        this.abbrv = abbrv;
        this.nameSpace = nameSpace;
    }

    @Override
    public String toString() {
        return "PREFIX " + abbrv + ":<" + nameSpace + ">";
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public URI getUri(String element) {
        return new URIImpl(this.nameSpace + element);
    }

    public boolean isValidResource(Resource resource) {
        return resource.stringValue().startsWith(nameSpace);
    }

    public Path getLocalResourcePath() throws URISyntaxException {
        return Paths.get(FileUtils
                .getResource(ontologiesProjectPath + this.localResourceName));
    }

    public URL getLocalResourceURL()
            throws MalformedURLException, URISyntaxException {
        return FileUtils
                .getResource(ontologiesProjectPath + this.localResourceName)
                .toURL();
    }
}
