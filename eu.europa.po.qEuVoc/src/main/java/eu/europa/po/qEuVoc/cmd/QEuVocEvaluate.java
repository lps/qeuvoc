/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 7 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.cmd;

import java.io.IOException;
import java.net.URISyntaxException;

import org.openrdf.OpenRDFException;

import com.beust.jcommander.ParameterException;

import at.ac.univie.mminf.qskos4j.cmd.VocEvaluate;
import at.ac.univie.mminf.qskos4j.util.vocab.InvalidRdfException;
import eu.europa.po.qEuVoc.QEuVoc;

/**
 * @author costeeu
 *
 */
public class QEuVocEvaluate extends VocEvaluate {

    /**
     * @param args
     * @throws OpenRDFException
     * @throws IOException
     * @throws URISyntaxException
     */
    public QEuVocEvaluate(String[] args)
            throws OpenRDFException, IOException, URISyntaxException {
        qskos = new QEuVoc();
        parseCmdParams(args);

        if (outputVersion) {
            System.out.println("Version: "
                    + getClass().getPackage().getImplementationVersion());
        }

        if (parsedCommand == null) {
            jc.usage();
            return;
        }
        try {
            listIssuesOrEvaluate();
        } catch (InvalidRdfException e) {
            System.err.println(
                    "!! Provided input file does not contain valid RDF data");
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        try {
            new QEuVocEvaluate(args);
        } catch (ParameterException paramExc) {
            jc.usage();
            System.err.println("!! " + paramExc.getMessage());
        } catch (IOException ioException) {
            System.err.println(
                    "!! Error reading file: " + ioException.getMessage());
        } catch (OpenRDFException rdfException) {
            System.err.println("!! Error processing vocabulary: "
                    + rdfException.getMessage());
        } catch (URISyntaxException e) {
            System.err.println(
                    "!! Error fetching the query file (project inner resource): "
                            + e.getMessage());
            e.printStackTrace();
        }
    }

}
