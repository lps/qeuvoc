/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 7 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import org.openrdf.OpenRDFException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import at.ac.univie.mminf.qskos4j.issues.Issue;
import eu.europa.po.qEuVoc.BindingSetCollectionResult;
import eu.europa.po.qEuVoc.util.vocab.EuVocPrefix;

/**
 * @author costeeu
 *
 */
public class ClassAndSchemeDefinition
        extends Issue<BindingSetCollectionResult> {

    /**
     * 
     * @param id
     * @param name
     * @param description
     * @param type
     * 
     *            The rich skos files (SKOS-AP-EU) must contain a definition for
     *            the table they represent both as OWL class and as
     *            skos:ConceptScheme. <br>
     *            All the skos:Concept instances must be also individuals
     *            belonging to the OWL class. <br>
     *            This test runs this query and checks whether there is exactly
     *            one pairing of class and scheme in the file. <br>
     *            <br>
     *            SELECT * WHERE { ?s a skos:ConceptScheme . ?c rdfs:subClassOf<br>
     *            skos:Concept ; owl:equivalentClass/owl:onProperty<br>
     *            skos:inScheme ; owl:equivalentClass/owl:hasValue ?s. }
     */
    public ClassAndSchemeDefinition() {
        super("ccd", "Class and Concept Scheme definition check",
                "Checks whether there is a single conceptscheme and a single class definition corresponding to that concept scheme",
                IssueType.ANALYTICAL);
    }

    /*
     * (non-Javadoc)
     * 
     * @see at.ac.univie.mminf.qskos4j.issues.Issue#invoke()
     */
    @Override
    protected BindingSetCollectionResult invoke() throws OpenRDFException {
        String queryString = "" + EuVocPrefix.OWL + " " + EuVocPrefix.SKOS + " "
                + EuVocPrefix.RDFS + " " + EuVocPrefix.RDF
                + "SELECT ?scheme ?class \r\n" + "WHERE \r\n" + "    {\r\n"
                + "        ?scheme a skos:ConceptScheme .\r\n"
                + "        ?class rdfs:subClassOf skos:Concept ;\r\n"
                + "            owl:equivalentClass/owl:onProperty skos:inScheme ;\r\n"
                + "            owl:equivalentClass/owl:hasValue ?scheme.\r\n"
                + "    }";
        TupleQuery select = repCon.prepareTupleQuery(QueryLanguage.SPARQL,
                queryString);

        BindingSetCollectionResult res = new BindingSetCollectionResult(
                select.evaluate());

        if (res.getData().size() == 1) {
            res.setProblematic(false);
        } else {
            res.setProblematic(true);
        }
        return res;
    }
}
