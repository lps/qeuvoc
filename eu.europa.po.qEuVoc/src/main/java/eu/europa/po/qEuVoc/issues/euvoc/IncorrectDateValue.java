/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 9 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import java.io.IOException;
import java.net.URISyntaxException;

import org.openrdf.OpenRDFException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;

import at.ac.univie.mminf.qskos4j.issues.Issue;
import at.ac.univie.mminf.qskos4j.issues.Issue.IssueType;
import eu.europa.po.qEuVoc.BindingSetCollectionResult;
import eu.europa.po.qEuVoc.issues.GenericQueryTupleIssue;
import eu.europa.po.qEuVoc.util.vocab.EuVocPrefix;

/**
 * @author costeeu tests whether the date related classes are provided a correct
 *         date value
 */
public class IncorrectDateValue extends GenericQueryTupleIssue {

    /**
     *  Test whether the xsd:date & xsd:dateTime properties are provided correct datatype.
     *  
     *            dct:created|dct:issued|dct:modified|dct:dateSubmitted|dct:
     *            dateAccepted|euvoc:startDate|euvoc:endDate <br>
     *            <br>
     *            SELECT ?individual ?date <br>
     *            WHERE <br>
     *            { ?individual
     *            dct:created|dct:issued|dct:modified|dct:dateSubmitted|dct:
     *            dateAccepted|euvoc:startDate|euvoc:endDate ?date .<br>
     *            FILTER (datatype(?date)!=xsd:date &&
     *            datatype(?date)!=xsd:dateTime)<br>
     *            }<br>
     * @throws IOException 
     * @throws URISyntaxException 
     */
    public IncorrectDateValue() throws URISyntaxException, IOException {
        super("dv", "Incorrect Date Values",
                "Checks whether the range values of the date datatype properties are in correct data type and format",
                IssueType.ANALYTICAL);
        setQueryFromResourceFile("dateDataType.rq");
    }

//    /*
//     * (non-Javadoc)
//     * 
//     * @see at.ac.univie.mminf.qskos4j.issues.Issue#invoke()
//     */
//    @Override
//    protected BindingSetCollectionResult invoke() throws OpenRDFException {
//        String queryString = "" + EuVocPrefix.XSD + " " + EuVocPrefix.EUVOC
//                + " " + EuVocPrefix.DCT + " " + EuVocPrefix.RDF
//                + "SELECT ?individual ?date \r\n" + "WHERE \r\n"
//                + "    {   ?individual dct:created|dct:issued|dct:modified|dct:dateSubmitted|dct:dateAccepted|euvoc:startDate|euvoc:endDate ?date .\r\n"
//                + "        FILTER (datatype(?date)!=xsd:date && datatype(?date)!=xsd:dateTime)\r\n"
//                + "    }";
//
//        TupleQuery select = repCon.prepareTupleQuery(QueryLanguage.SPARQL,
//                queryString);
//
//        BindingSetCollectionResult res = new BindingSetCollectionResult(
//                select.evaluate());
//
//        return res;
//    }
}
