/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 8 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import java.io.IOException;
import java.net.URISyntaxException;

import eu.europa.po.qEuVoc.issues.GenericQueryTupleIssue;

/**
 * @author costeeu
 *
 */
public class StartEndDates extends GenericQueryTupleIssue {

    /**
     * PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> <br>
     * PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#><br>
     * PREFIX skos: <http://www.w3.org/2004/02/skos/core#><br>
     * PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#><br>
     * PREFIX xsd: <http://www.w3.org/2001/XMLSchema#><br>
     * PREFIX owl: <http://www.w3.org/2002/07/owl#><br>
     * PREFIX euvoc: <http://publications.europa.eu/ontology/euvoc#><br>
     * PREFIX dct: <http://purl.org/dc/terms/> <br>
     * <br>
     * SELECT ?individual ?startDate ?endDate ?issued ?modified ?dateSubmitted
     * ?dateAccepted<br>
     * WHERE <br>
     * { <br>
     * #individuals with endDate without start date<br>
     * {<br>
     * ?individual euvoc:endDate ?endDate .<br>
     * FILTER NOT EXISTS {?individual euvoc:startDate ?startDate}<br>
     * }<br>
     * UNION <br>
     * #individuals start date after end date<br>
     * {<br>
     * ?individual euvoc:startDate ?startDate .<br>
     * ?individual euvoc:endDate ?endDate .<br>
     * FILTER (?endDate <= ?startDate)<br>
     * }<br>
     * UNION<br>
     * #individuals with dateAccepted without dateSubmitted<br>
     * {<br>
     * ?individual dct:dateAccepted ?dateAccepted .<br>
     * FILTER NOT EXISTS {?individual dct:dateSubmitted ?dateSubmitted}<br>
     * }<br>
     * UNION<br>
     * #individuals with dateAccepted < dateSubmitted<br>
     * {<br>
     * ?individual dct:dateSubmitted ?dateSubmitted .<br>
     * ?individual dct:dateAccepted ?dateAccepted .<br>
     * FILTER (?dateAccepted <= ?dateSubmitted)<br>
     * }<br>
     * UNION<br>
     * #individuals with modified without issued<br>
     * {<br>
     * ?individual dct:modified ?modified .<br>
     * FILTER NOT EXISTS {?individual dct:issued ?issued}<br>
     * }<br>
     * UNION <br>
     * #individuals with modified < issued<br>
     * {<br>
     * ?individual dct:issued ?issued .<br>
     * ?individual dct:modified ?modified .<br>
     * FILTER ( ?modified <= ?issued ) <br>
     * }<br>
     * }<br>
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    public StartEndDates(IncorrectDateValue dateValues)
            throws URISyntaxException, IOException {
        super(dateValues, "sed", "Incorrect Start and End dates",
                "Checks whether the end date is after the start date",
                IssueType.ANALYTICAL);
        setQueryFromResourceFile("dateOrder.rq");
    }

    public StartEndDates() throws URISyntaxException, IOException {
        super(new IncorrectDateValue(), "sed", "Incorrect Start and End dates",
                "Checks whether the end date is after the start date",
                IssueType.ANALYTICAL);
        setQueryFromResourceFile("dateOrder.rq");
    }

//    /*
//     * (non-Javadoc)
//     * 
//     * @see at.ac.univie.mminf.qskos4j.issues.Issue#invoke()
//     */
//    @Override
//    protected BindingSetCollectionResult invoke() throws OpenRDFException {
//        String queryString = "" + EuVocPrefix.XSD + " " + EuVocPrefix.EUVOC
//                + " " + EuVocPrefix.DCT + " " + EuVocPrefix.RDF
//                + "SELECT ?individual ?startDate ?endDate ?issued ?modified ?dateSubmitted ?dateAccepted \r\n"
//                + "WHERE \r\n" + "{   \r\n"
//                + "  #individuals with endDate without start date\r\n"
//                + "  {\r\n" + "    ?individual euvoc:endDate ?endDate . \r\n"
//                + "    FILTER NOT EXISTS {?individual euvoc:startDate ?startDate} \r\n"
//                + "    BIND ( \"X\" AS ?startDate)" + "  }\r\n" + "  UNION \r\n"
//                + "  #individuals start date after end date\r\n" + "  { \r\n"
//                + "    ?individual euvoc:startDate ?startDate . \r\n"
//                + "    ?individual euvoc:endDate ?endDate . \r\n"
//                + "    FILTER (?endDate <= ?startDate) \r\n" + "  }\r\n"
//                + "  UNION\r\n"
//                + "  #individuals with dateAccepted without dateSubmitted\r\n"
//                + "  {\r\n"
//                + "    ?individual dct:dateAccepted ?dateAccepted . \r\n"
//                + "    FILTER NOT EXISTS {?individual dct:dateSubmitted ?dateSubmitted} \r\n"
//                + "    BIND ( \"X\" AS ?dateSubmitted)" + "  }\r\n"
//                + "  UNION \r\n"
//                + "  #individuals with dateAccepted < dateSubmitted\r\n"
//                + "  {\r\n"
//                + "    ?individual dct:dateSubmitted ?dateSubmitted . \r\n"
//                + "    ?individual dct:dateAccepted ?dateAccepted . \r\n"
//                + "    FILTER (?dateAccepted <= ?dateSubmitted) \r\n"
//                + "  }\r\n" + "  UNION\r\n"
//                + "  #individuals with modified without issued\r\n" + "  {\r\n"
//                + "    ?individual dct:modified ?modified . \r\n"
//                + "    FILTER NOT EXISTS {?individual dct:issued ?issued} \r\n"
//                + "    BIND ( \"X\" AS ?issued)" + "  }\r\n" + "  UNION \r\n"
//                + "  #individuals with modified < issued\r\n" + "  {\r\n"
//                + "    ?individual dct:issued ?issued . \r\n"
//                + "    ?individual dct:modified ?modified . \r\n"
//                + "    FILTER ( ?modified <= ?issued ) \r\n" + "  }\r\n" + "}";
//
//        TupleQuery select = repCon.prepareTupleQuery(QueryLanguage.SPARQL,
//                queryString);
//
//        BindingSetCollectionResult res = new BindingSetCollectionResult(
//                select.evaluate());
//        return res;
//    }

}
