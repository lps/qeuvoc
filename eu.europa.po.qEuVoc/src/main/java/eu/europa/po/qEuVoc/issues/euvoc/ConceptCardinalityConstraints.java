/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 9 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import java.io.IOException;
import java.net.URISyntaxException;

import eu.europa.po.qEuVoc.issues.GenericQueryTupleIssue;

/**
 * TODO: this issue returns different results than the query run within in
 * Sesame Work-Bench. Needs further debugging.
 * 
 * @author costeeu
 */
public class ConceptCardinalityConstraints extends GenericQueryTupleIssue {

    /**
     * PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> <br>
     * PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#><br>
     * PREFIX skos: <http://www.w3.org/2004/02/skos/core#><br>
     * PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#><br>
     * PREFIX xsd: <http://www.w3.org/2001/XMLSchema#><br>
     * PREFIX owl: <http://www.w3.org/2002/07/owl#><br>
     * PREFIX euvoc: <http://publications.europa.eu/ontology/euvoc#><br>
     * PREFIX dct: <http://purl.org/dc/terms/> <br>
     * <br>
     * SELECT<br>
     * ?individual<br>
     * (COUNT(?is) AS ?inScheme)<br>
     * (COUNT(?tc) AS ?topConceptOf) <br>
     * (COUNT(?pl) AS ?prefLabel)<br>
     * (COUNT(?stat) AS ?status)<br>
     * (COUNT(?vi) AS ?versionInfo)<br>
     * (COUNT(?cre) AS ?created)<br>
     * (COUNT(?start) AS ?startDate)<br>
     * (COUNT(?end) AS ?endDate)<br>
     * <br>
     * (COUNT(?ds) AS ?dateSubmitted)<br>
     * (COUNT(?da) AS ?dateAccepterd)<br>
     * <br>
     * WHERE<br>
     * { <br>
     * ?individual a skos:Concept .<br>
     * {?individual skos:inScheme ?is .}<br>
     * UNION<br>
     * {?individual skos:topConceptOf ?tc .}<br>
     * UNION<br>
     * {?individual skos:prefLabel ?pl .}<br>
     * UNION<br>
     * {?individual euvoc:status ?stat .}<br>
     * UNION<br>
     * {?individual owl:versionInfo ?vi .}<br>
     * UNION<br>
     * {?individual dct:created ?cre .}<br>
     * UNION<br>
     * {?individual euvoc:startDate ?start .}<br>
     * UNION<br>
     * {?individual euvoc:endDate ?end .}<br>
     * UNION <br>
     * {?individual dct:dateSubmitted ?ds .}<br>
     * UNION <br>
     * {?individual dct:dateAccepted ?da .}<br>
     * }<br>
     * GROUP BY ?individual<br>
     * HAVING (COUNT(?is) != 1 ||<br>
     * COUNT(?tc)>1 || <br>
     * COUNT(?pl)<1 || <br>
     * COUNT(?stat)!=1 ||<br>
     * COUNT(?vi)!=1 || <br>
     * COUNT(?cre)!=1 || <br>
     * COUNT(?start) !=1 ||<br>
     * COUNT(?end)>1 ||<br>
     * COUNT(?ds)>1 ||<br>
     * COUNT(?da)>1<br>
     * )<br>
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    public ConceptCardinalityConstraints()
            throws URISyntaxException, IOException {
        super("ccc", "Concept Cardinality Constraints",
                " Check the cardinality constraints defined in the EuVoc SKOS AP.",
                IssueType.ANALYTICAL);
        setQueryFromResourceFile("conceptCardinality.rq");
    }
}
