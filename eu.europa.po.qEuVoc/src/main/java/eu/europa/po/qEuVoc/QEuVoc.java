/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 7 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.openrdf.OpenRDFException;

import at.ac.univie.mminf.qskos4j.QSkos;
import eu.europa.po.qEuVoc.issues.euvoc.ClassAndSchemeDefinition;
import eu.europa.po.qEuVoc.issues.euvoc.ConceptCardinalityConstraints;
import eu.europa.po.qEuVoc.issues.euvoc.IncorrectDateValue;
import eu.europa.po.qEuVoc.issues.euvoc.StartEndDates;

/**
 * @author costeeu
 *  TODO:consider implementing https://code.google.com/p/j-text-utils/ for reports
 */
public class QEuVoc extends QSkos {

    /**
     * @param file
     * @throws OpenRDFException
     * @throws IOException
     * @throws URISyntaxException 
     */
    public QEuVoc(File file) throws OpenRDFException, IOException, URISyntaxException {
        super(file);
        addEuVocIssues();
    }


    public QEuVoc() throws URISyntaxException, IOException {
        super();
        addEuVocIssues();
    }
    
    /**
     * @throws IOException 
     * @throws URISyntaxException 
     * 
     */
    private void addEuVocIssues() throws URISyntaxException, IOException {
        ClassAndSchemeDefinition classSchemeIssue = new ClassAndSchemeDefinition();
        this.registeredIssues.add(classSchemeIssue);
        
        IncorrectDateValue dateValue = new IncorrectDateValue();
        this.registeredIssues.add(dateValue);
        
        StartEndDates startEndDates = new StartEndDates(dateValue);
        this.registeredIssues.add(startEndDates);
        
        ConceptCardinalityConstraints conceptCardinalityConstraints = new ConceptCardinalityConstraints();
        this.registeredIssues.add(conceptCardinalityConstraints);
    }
    
}
