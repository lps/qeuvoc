/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 7 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.openrdf.OpenRDFException;

/**
 * @author costeeu
 *
 */
public class QEuVocException extends OpenRDFException {

    private static final long serialVersionUID = 2284659429025426503L;
    
    public static String stackTraceToString(Throwable t){
        StringWriter sw = new StringWriter();
        t.printStackTrace( new PrintWriter(sw));
        return sw.toString();
    }

    /**
     * 
     */
    public QEuVocException() {
    }

    /**
     * @param msg
     */
    public QEuVocException(String msg) {
        super(msg);
    }

    /**
     * @param t
     */
    public QEuVocException(Throwable t) {
        super(t);
    }

    /**
     * @param msg
     * @param t
     */
    public QEuVocException(String msg, Throwable t) {
        super(msg, t);
    }

    
}
