/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 9 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import java.io.IOException;
import java.net.URISyntaxException;

import eu.europa.po.qEuVoc.issues.GenericQueryTupleIssue;

/**
 * @author costeeu
 *
 */
public class DatasetCardinalityConstraints extends GenericQueryTupleIssue {

    /**
     * @param name
     * @param description
     * @param type
     * @throws IOException 
     * @throws URISyntaxException 
     */
    public DatasetCardinalityConstraints() throws URISyntaxException, IOException {
        super("ccc", "Dataset Cardinality Constraints",
                " Check the cardinality constraints defined in the EuVoc SKOS AP.",
                IssueType.ANALYTICAL);
        setQueryFromResourceFile("datasetCardinality.rq");
    }
}
