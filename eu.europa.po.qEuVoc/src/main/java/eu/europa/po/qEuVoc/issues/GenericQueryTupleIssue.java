/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 9 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues;

import java.io.IOException;
import java.net.URISyntaxException;

import org.openrdf.OpenRDFException;
import org.openrdf.model.URI;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;

import at.ac.univie.mminf.qskos4j.issues.Issue;
import eu.europa.po.qEuVoc.BindingSetCollectionResult;
import eu.europa.po.qEuVoc.util.FileUtils;

/**
 * @author costeeu
 *
 */
public class GenericQueryTupleIssue extends Issue<BindingSetCollectionResult> {

    protected String query = null;

    /**
     * @param name
     * @param description
     * @param type
     */
    public GenericQueryTupleIssue(String name, String description,
            IssueType type) {
        super(name, description, type);
    }

    /**
     * @param id
     * @param name
     * @param description
     * @param type
     */
    public GenericQueryTupleIssue(String id, String name, String description,
            IssueType type) {
        super(id, name, description, type);
    }

    /**
     * @param dependentIssue
     * @param id
     * @param name
     * @param description
     * @param type
     */
    public GenericQueryTupleIssue(Issue dependentIssue, String id, String name,
            String description, IssueType type) {
        super(dependentIssue, id, name, description, type);
    }

    /**
     * @param id
     * @param name
     * @param description
     * @param type
     * @param weblink
     */
    public GenericQueryTupleIssue(String id, String name, String description,
            IssueType type, URI weblink) {
        super(id, name, description, type, weblink);
    }

    /**
     * @param dependentIssue
     * @param id
     * @param name
     * @param description
     * @param type
     * @param weblink
     */
    public GenericQueryTupleIssue(Issue dependentIssue, String id, String name,
            String description, IssueType type, URI weblink) {
        super(dependentIssue, id, name, description, type, weblink);
    }

    public void setQueryFromText(String query) {
        this.query = query;
    }
    
    public void setQueryFromResourceFile(String resource) throws URISyntaxException, IOException {
        this.query = FileUtils.readTextResourceFile(resource);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see at.ac.univie.mminf.qskos4j.issues.Issue#invoke()
     */
    @Override
    protected BindingSetCollectionResult invoke() throws OpenRDFException {
        TupleQuery select = repCon.prepareTupleQuery(QueryLanguage.SPARQL,
                this.query);
        BindingSetCollectionResult res = new BindingSetCollectionResult(
                select.evaluate());
        return res;
    }

}
