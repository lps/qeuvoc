/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 8 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.util;

import java.util.ArrayList;
import java.util.Collection;
import org.openrdf.OpenRDFException;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;

/**
 * @author costeeu an extension of
 *         at.ac.univie.mminf.qskos4j.util.TupleQueryResultUtil
 */
public class QueryResultUtil {

    /**
     * Transforms a Tuple queryResult into a Collection of BindingSets.
     * 
     * @param result
     * @return
     * @throws QueryEvaluationException
     * 
     * 
     */
    public static Collection<BindingSet> tupleQueryResultToCollection(
            TupleQueryResult result) throws OpenRDFException {
        Collection<BindingSet> resultCollection = new ArrayList<BindingSet>();
        while (result.hasNext()) {
            resultCollection.add(result.next());
        }
        return resultCollection;
    }
}
