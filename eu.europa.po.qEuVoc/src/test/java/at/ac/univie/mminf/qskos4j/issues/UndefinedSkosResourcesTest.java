package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.skosintegrity.UndefinedSkosResources;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;
import org.openrdf.model.URI;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

public class UndefinedSkosResourcesTest {

    private UndefinedSkosResources undefinedSkosResourcesInConcepts,
            undefinedSkosResourcesInDeprecatedAndIllegal;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        undefinedSkosResourcesInConcepts = new UndefinedSkosResources();
        undefinedSkosResourcesInConcepts
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource("qskos/concepts.rdf")
                        .getConnection());
        undefinedSkosResourcesInDeprecatedAndIllegal = new UndefinedSkosResources();
        undefinedSkosResourcesInDeprecatedAndIllegal
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource(
                                "qskos/deprecatedAndIllegalTerms.rdf")
                        .getConnection());
    }

    @Test
    public void testUndefinedSkosResourcesCount_1() throws OpenRDFException {
        Collection<URI> undefRes = undefinedSkosResourcesInConcepts.getResult()
                .getData();
        Assert.assertEquals(3, undefRes.size());
    }

    @Test
    public void testUndefinedSkosResourcesCount_2() throws OpenRDFException {
        Collection<URI> undefRes = undefinedSkosResourcesInDeprecatedAndIllegal
                .getResult().getData();
        Assert.assertEquals(12, undefRes.size());
    }

}
