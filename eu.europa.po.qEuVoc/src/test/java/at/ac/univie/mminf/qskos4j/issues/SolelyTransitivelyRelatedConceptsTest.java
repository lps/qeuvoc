package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.relations.SolelyTransitivelyRelatedConcepts;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

public class SolelyTransitivelyRelatedConceptsTest {

    private SolelyTransitivelyRelatedConcepts solelyTransitivelyRelatedConcepts;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        solelyTransitivelyRelatedConcepts = new SolelyTransitivelyRelatedConcepts();
        solelyTransitivelyRelatedConcepts
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource(
                                "qskos/solitaryTransitiveRelations.rdf")
                        .getConnection());
    }

    @Test
    public void testSolitaryTransitiveRelationsCount() throws OpenRDFException {
        Assert.assertEquals(4,
                solelyTransitivelyRelatedConcepts.getResult().getData().size());
    }

}
