package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.conceptscheme.ConceptSchemes;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by christian
 * Date: 26.01.13
 * Time: 14:47
 */
public class ConceptSchemesTest {

    private ConceptSchemes conceptSchemes;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        conceptSchemes = new ConceptSchemes();
        conceptSchemes.setRepositoryConnection(new RepositoryBuilder().setUpFromTestResource("qskos/aggregations.rdf").getConnection());
    }

    @Test
    public void testLexicalRelationsCount() throws OpenRDFException
    {
        Assert.assertEquals(3, conceptSchemes.getResult().getData().size());
    }
}
