package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.outlinks.HttpURIs;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by christian Date: 26.01.13 Time: 15:33
 */
public class HttpURIsTest {

    private HttpURIs httpURIs1, httpURIs2;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        httpURIs1 = new HttpURIs();
        httpURIs1.setRepositoryConnection(new RepositoryBuilder()
                .setUpFromTestResource("qskos/concepts.rdf").getConnection());

        httpURIs2 = new HttpURIs();
        httpURIs2.setRepositoryConnection(new RepositoryBuilder()
                .setUpFromTestResource("qskos/resources.rdf").getConnection());
    }

    @Test
    public void testConceptsHttpUriCount() throws OpenRDFException {
        Assert.assertEquals(21, httpURIs1.getResult().getData().size());
    }

    @Test
    public void testResourcesHttpUriCount() throws OpenRDFException {
        Assert.assertEquals(8, httpURIs2.getResult().getData().size());
    }
}
