package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.outlinks.HttpUriSchemeViolations;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by christian Date: 26.01.13 Time: 17:15
 */
public class NonHttpResourcesTest {

    private HttpUriSchemeViolations httpUriSchemeViolationsForConcepts,
            httpResourcesForUriSchemeViolations;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        httpUriSchemeViolationsForConcepts = new HttpUriSchemeViolations();
        httpUriSchemeViolationsForConcepts
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource("qskos/concepts.rdf")
                        .getConnection());

        httpResourcesForUriSchemeViolations = new HttpUriSchemeViolations();
        httpResourcesForUriSchemeViolations
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource("qskos/resources.rdf")
                        .getConnection());
    }

    @Test
    public void testConceptsNonHttpUriCount() throws OpenRDFException {
        Assert.assertEquals(1, httpUriSchemeViolationsForConcepts.getResult()
                .getData().size());
    }

    @Test
    public void testResourcesNonHttpUriCount() throws OpenRDFException {
        Assert.assertEquals(4, httpResourcesForUriSchemeViolations.getResult()
                .getData().size());
    }
}
