package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.cycles.HierarchicalCycles;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by christian Date: 27.01.13 Time: 00:20
 */
public class HierarchicalCyclesTest {

    private HierarchicalCycles hierarchicalCycles,
            hierarchicalCyclesForComponents;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        hierarchicalCycles = new HierarchicalCycles(
                new HierarchyGraphBuilder());
        hierarchicalCycles.setRepositoryConnection(new RepositoryBuilder()
                .setUpFromTestResource("qskos/cycles.rdf").getConnection());

        hierarchicalCyclesForComponents = new HierarchicalCycles(
                new HierarchyGraphBuilder());
        hierarchicalCyclesForComponents
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource("qskos/components.rdf")
                        .getConnection());
    }

    @Test
    public void testCycleCount() throws OpenRDFException {
        Assert.assertEquals(3, hierarchicalCycles.getResult().getData().size());
    }

    @Test
    public void testComponentsCycleCount() throws OpenRDFException {
        Assert.assertEquals(3,
                hierarchicalCyclesForComponents.getResult().getData().size());
    }

}
