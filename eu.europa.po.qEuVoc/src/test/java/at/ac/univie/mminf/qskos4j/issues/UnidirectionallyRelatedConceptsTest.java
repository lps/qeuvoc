package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.concepts.AuthoritativeConcepts;
import at.ac.univie.mminf.qskos4j.issues.concepts.InvolvedConcepts;
import at.ac.univie.mminf.qskos4j.issues.relations.UnidirectionallyRelatedConcepts;
import at.ac.univie.mminf.qskos4j.util.Tuple;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;
import org.openrdf.model.Resource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

public class UnidirectionallyRelatedConceptsTest {

    private UnidirectionallyRelatedConcepts unidirectionallyRelatedConcepts;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        unidirectionallyRelatedConcepts = new UnidirectionallyRelatedConcepts(
                new AuthoritativeConcepts(new InvolvedConcepts()));
        unidirectionallyRelatedConcepts
                .setRepositoryConnection(new RepositoryBuilder()
                        .setUpFromTestResource(
                                "qskos/omittedInverseRelations.rdf")
                        .getConnection());
    }

    @Test
    public void testMissingInverseRelationsCount() throws OpenRDFException {
        Map<Tuple<Resource>, String> missingRelations = unidirectionallyRelatedConcepts
                .getResult().getData();
        Assert.assertEquals(8, missingRelations.size());
    }
}
