package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.skosintegrity.RelationClashes;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

public class RelationClashesTest {

    private RelationClashes relationClashes;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        relationClashes = new RelationClashes(new HierarchyGraphBuilder());
        relationClashes.setRepositoryConnection(new RepositoryBuilder()
                .setUpFromTestResource("qskos/relationClashes.rdf")
                .getConnection());
    }

    @Test
    public void testAssociativeVsHierarchicalClashes() throws OpenRDFException {
        Assert.assertEquals(10, relationClashes.getResult().getData().size());
    }

}
