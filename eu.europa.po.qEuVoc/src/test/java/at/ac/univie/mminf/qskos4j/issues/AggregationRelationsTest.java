package at.ac.univie.mminf.qskos4j.issues;

import at.ac.univie.mminf.qskos4j.issues.count.AggregationRelations;
import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by christian
 * Date: 26.01.13
 * Time: 14:47
 */
public class AggregationRelationsTest {

    private AggregationRelations aggregationRelations;

    @Before
    public void setUp() throws OpenRDFException, IOException, URISyntaxException {
        aggregationRelations = new AggregationRelations();
        aggregationRelations.setRepositoryConnection(new RepositoryBuilder().setUpFromTestResource("qskos/aggregations.rdf").getConnection());
    }

    @Test
    public void testAggregationRelationsCount() throws OpenRDFException
    {
        Assert.assertEquals(6, aggregationRelations.getResult().getData().longValue());
    }

}
