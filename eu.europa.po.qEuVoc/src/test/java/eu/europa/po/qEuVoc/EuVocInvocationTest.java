/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 5 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc;

import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

import at.ac.univie.mminf.qskos4j.cmd.VocEvaluate;
import eu.europa.po.qEuVoc.cmd.QEuVocEvaluate;

/**
 * @author costeeu
 *
 */
public class EuVocInvocationTest {

    @Test
    public void test() {
        String[] commands = { VocEvaluate.CMD_NAME_ANALYZE, VocEvaluate.CMD_NAME_SUMMARIZE };
        for (String command : commands) {
            try {
                new QEuVocEvaluate(new String[] {command, "-dc", "mil, bl", "-o", "/tmp/testreport_"+command+".txt", getTestFileName() });
//                new QEuVocEvaluate(new String[] { command, "-c", "ccc","-o", "/tmp/testreport_"+command+".txt",getTestFileName() });
            } catch (Exception e) {
                Assert.fail(e.getMessage() + ", command: " + command);
            }
        }
    }

    private String getTestFileName() {
        URL conceptsUrl = getClass().getResource("/euvoc/humansexes-rich-faulty.rdf");
        return conceptsUrl.getFile();
    }
}
