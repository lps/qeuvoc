/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 9 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.issues.euvoc;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.OpenRDFException;

import at.ac.univie.mminf.qskos4j.util.vocab.RepositoryBuilder;
import eu.europa.po.qEuVoc.BindingSetCollectionResult;

/**
 * @author costeeu
 *
 */
public class IncorrectDateValueTest {
    
    
    private IncorrectDateValue dateValue;
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        dateValue = new IncorrectDateValue();
        dateValue.setRepositoryConnection(new RepositoryBuilder().setUpFromTestResource("euvoc/humansexes-rich-faulty.rdf").getConnection());
    }

    @Test
    public void test() throws OpenRDFException {
        BindingSetCollectionResult result = dateValue.getResult();
        Assert.assertEquals(8, result.occurrenceCount());
        assertTrue(result.isProblematic());
    }

}
