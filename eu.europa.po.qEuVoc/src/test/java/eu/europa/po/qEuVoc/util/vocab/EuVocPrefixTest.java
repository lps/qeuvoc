/**
 *  eu.europa.po.qEuVoc
 *
 *  Copyright 2015 by Eugeniu Costetchi <costezki.eugen@gmail.com>
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *  Some rights reserved. See COPYING, AUTHORS.
 *
 * @license GPL-3.0 <http://opensource.org/licenses/GPL-3.0>
 * @author Eugeniu Costetchi
 * @date 15 Oct 2015
 **/

/**
 * 
 */
package eu.europa.po.qEuVoc.util.vocab;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.LinkOption;

import org.junit.Before;
import org.junit.Test;

/**
 * @author costeeu
 *
 */
public class EuVocPrefixTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testADMS() {
        try {
            assertTrue(Files.exists(EuVocPrefix.ADMS.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testDCAT() {
        try {
            assertTrue(Files.exists(EuVocPrefix.DCAT.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testDCelements() {
        try {
            assertTrue(Files.exists(EuVocPrefix.DC.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testDCTerms() {
        try {
            assertTrue(Files.exists(EuVocPrefix.DCT.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testEueuVoc() {
        try {
            assertTrue(Files.exists(EuVocPrefix.EUVOC.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testFOAF() {
        try {
            assertTrue(Files.exists(EuVocPrefix.FOAF.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLemon() {
        try {
            assertTrue(Files.exists(EuVocPrefix.LEMON.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLexInfo() {
        try {
            assertTrue(Files.exists(EuVocPrefix.LEXINFO.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testLexVo() {
        try {
            assertTrue(Files.exists(EuVocPrefix.LEXVO.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testORG() {
        try {
            assertTrue(Files.exists(EuVocPrefix.ORG.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPROV() {
        try {
            assertTrue(Files.exists(EuVocPrefix.PROV.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testSKOSXL() {
        try {
            assertTrue(Files.exists(EuVocPrefix.SKOSXL.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testSKOS() {
        try {
            assertTrue(Files.exists(EuVocPrefix.SKOS.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testVCARD() {
        try {
            assertTrue(Files.exists(EuVocPrefix.VCARD.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testVOID() {
        try {
            assertTrue(Files.exists(EuVocPrefix.VOID.getLocalResourcePath(),
                    LinkOption.NOFOLLOW_LINKS));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fail();
        }
    }
}
